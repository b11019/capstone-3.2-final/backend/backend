const mongoose = require('mongoose');
const userSchema = mongoose.Schema({
    
    username: {
        type: String,
        required : true
    },
    firstName: {
        type: String,
        required : true
    },
    lastName: {
        type: String,
        required : true
    },
    email: {
        type: String,
        required : true
    },
    password: {
        type: String,
        required : true
    },
    isAdmin: {
        type: Boolean,
        default : false,
       
    },
    cart :[{
        productId : {
            type: String,
            required : true
        },
        productName : {
            type: String,
            required : true
        },
        productDescription : {
            type: String,
            required : true
        },
        price : {
            type: Number,
            required : true
        },
       
    }],
    totalAmount: [{
        type: Number
        
    }]

});

module.exports = mongoose.model('User', userSchema);
