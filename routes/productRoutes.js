//! [SECTION DEPENDENCIES]
const express = require('express');
const router = express.Router();
const auth = require('../auth');
const { verify, verifyAdmin } = auth;

// ! CONTROLLER DEPENDENCIES
const productControllers = require('../controllers/productControllers');




// CREATE PRODUCT ADMIN ONLY
router.post('/', verify, verifyAdmin, productControllers.addProduct);

// GET ALL PRODUCTS
router.get('/getAllProducts', productControllers.getAllProducts);

// SEARCH PRODUCTS
router.post('/search', productControllers.search);
// SORT ASCENDING
router.get('/sortAscending', productControllers.sortAscending);
// SORT DESCENDING
router.get('/sortDescending', productControllers.sortDescending);

// GET PRODUCTS BY CATEGORY
router.post('/category', productControllers.category);

// Deactivate Product
router.put('/deactProduct/:id',  productControllers.deactivateProduct);

// View Specific Product
router.get('/viewProduct/:id', productControllers.viewProduct);

// ADMIN MANAGE PRODUCTS
router.get('/admin', verify,verifyAdmin, productControllers.manageDashBoard)


module.exports = router;
