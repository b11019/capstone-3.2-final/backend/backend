const User = require('../models/User');
const bcrypt = require('bcryptjs');
const auth = require('../auth');
// const { reset } = require('nodemon');
const Product = require('../models/Product');

// REGISTER USER
module.exports.registerUser = (req, res) => {
    console.log(req.body);

    User.findOne({ email: req.body.email }).then(result => {
        console.log(result);

        if (result !== null && result.email == req.body.email) {
            return res.send('Account is already registered')
        } else {
            const hashedPW = bcrypt.hashSync(req.body.password, 10)
            let newUser = new User({
                username : req.body.username,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                password: hashedPW,
            })

            newUser.save()
                .then(result => res.send(result))
                .catch(err => res.send(err))
        }
    })
}

//!! CHECK EMAIL EXISTS
module.exports.checkEmailExists = (req, res) => {
    console.log(req.body.email)
    User.find({ email: req.body.email })
        .then(result => {
            if (result.length === 0) {
                return res.send(false)
            } else {
                res.send(true)
            }
        })
        .catch((err) => res.send(err))
}


//!! GET ALL USERS
module.exports.getAllUsers = (req, res) => {
    User.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err))
};

//! LOG IN USER / USER AUTHENTICATION
module.exports.login = (req, res) => {
    console.log(req.body);
    User.findOne({userName : req.body.userName})
    .then(foundUser => {
        if(foundUser === null) { 
            res.send(false)
        
        }else {
            const correctPassword = bcrypt.compareSync(req.body.password, foundUser.password)
            console.log(correctPassword);
            
            if(correctPassword){
                
                return res.send({accessToken: auth.createAccessToken(foundUser)})
            } else {
                return res.send(false)
            }
        }
    })
}


//! MAKE ADMIN
module.exports.makeAdmin = (req, res) => {
    console.log(req.user.id);
    console.log(req.params.id);

    let updates = {

        isAdmin: true
    }
    User.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then(makeAdmin => res.send(makeAdmin))
        .catch(err => res.send(err));
};



//! GET SINGLE USER DETAILS [ADMIN]
module.exports.getSingleDetails = (req, res) => {
    console.log(req.body)
    console.log(req.user.id)

    User.findById(req.user.id)
        .then(result => res.send(result))
        .catch(err => res.send(err))
};

//! ORDER A PRODUCT


module.exports.order = async(req, res) => {
    console.log(req.user.id)
    console.log(req.body.productId)

    if (req.user.isAdmin === null) {
        return res.send("Action Forbidden")
    }

    let isUserUpdated = await User.findById(req.user.id).then(user => {
        console.log(user)

        let newProduct = {
            productId: req.body.productId,
            productName: req.body.productName,
            productDescription: req.body.productDescription,
            price: req.body.price
        }

        user.cart.push(newProduct);
        user.totalAmount.push(newProduct.price);

        console.log(user.products);
        let sum = user.totalAmount.reduce((prev, current) => prev + current);

        User.findByIdAndUpdate(
            req.user.id, { Total: sum }, { new: true }
        )

        .then((user) => user)
            .catch((err) => err.message);

        return user.save()
            .then(user => true)
            .catch(err => err.message)
    })

    if (isUserUpdated !== true) {
        return res.send({ message: isUserUpdated })
    }

    let isProductUpdated = await Product.findById(req.body.productId).then(product => {
        console.log(product)

        let newOrders = {
            userId : req.user.id
        }

        product.orders.push(newOrders)


        return product.save()
            .then(product => true)
            .catch(err => err.message)
    })

    if (isProductUpdated !== true) {
        return res.send({ message: isProductUpdated })
    }
    if (isUserUpdated && isProductUpdated) {
        return res.send({ message: "Product checkout successful" })
    }
};

    


// GET ORDERS

module.exports.getUserDetails = (req, res) => {
    // console.log(req.user.id)
    User.findById(req.params.id)
        .then(result => res.send(result))
        .catch(err => res.send(err))
}




